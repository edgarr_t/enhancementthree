const express = require('express');
const router = express.Router();
const stock_controller = require('../controllers/stocks.controller');
var VerifyToken = require('../auth/VerifyToken');

router.get('/test', VerifyToken, stock_controller.test);
router.post('/createStock', VerifyToken, stock_controller.stock_create);
router.get('/id/:id', VerifyToken, stock_controller.stock_details_by_id);
router.get('/ticker/:id', VerifyToken, stock_controller.stock_details);
router.get('/portfolio/:company',VerifyToken,stock_controller.stock_portfolio);
router.get('/industryReport/:industry',VerifyToken,stock_controller.industry_details);
router.delete('/ticker/:id/delete', VerifyToken, stock_controller.stock_delete);
router.put('/ticker/:id/update', VerifyToken, stock_controller.stock_update);
router.post('/stockReports', VerifyToken, stock_controller.stock_report);
router.get('/sharesOutstanding/sector/:id', VerifyToken, stock_controller.get_shares_outstanding);
router.get('/sharesMovingAvg/:min/:max', VerifyToken, stock_controller.get_shares_avg);
router.get('/profitMargin/:min/:max', VerifyToken, stock_controller.get_profit_margin);
router.get('/countryReport/:id', VerifyToken, stock_controller.get_country_report);
router.get('/priceRange/:min/:max', VerifyToken, stock_controller.get_price_range);


module.exports = router;